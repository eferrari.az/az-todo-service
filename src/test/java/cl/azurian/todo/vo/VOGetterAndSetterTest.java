package cl.azurian.todo.vo;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Collections;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.beanrunner.BeanRunner;

public class VOGetterAndSetterTest {

	private static final String FAIL_MESSAGE = "Error running bean [%s]: %s";

	private static final Logger LOG = LoggerFactory.getLogger(VOGetterAndSetterTest.class);

	private static BeanRunner beanRunner;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		beanRunner = new BeanRunner();
		enrichRunner(beanRunner);
	}

	private static void enrichRunner(BeanRunner runner) {
		runner.addTestValue(Set.class, Collections.emptySet());
		runner.addTestValue(Boolean.class, Boolean.TRUE);
	}

	private void run(Object bean) {
		LOG.info(String.format("Testing instance of %s", bean.getClass().getName()));
		try {
			beanRunner.testBean(bean);
			bean.toString();
		} catch (Exception e) {
			LOG.error("Error ejecutando testComuna()", e);
			fail(String.format(FAIL_MESSAGE, bean.getClass().getName(), e.getMessage()));
		}
	}

	@Test
	void testTaskVO() {
		run(new TaskVO(0, "mock task", false, 0));
	}

	@Test
	void testTaskListVO() {
		run(new TaskListVO(0, "mock list", Collections.emptySet()));
	}
}
