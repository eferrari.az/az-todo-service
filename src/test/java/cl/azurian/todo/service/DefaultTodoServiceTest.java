package cl.azurian.todo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;

import cl.azurian.todo.exceptions.TodoException;
import cl.azurian.todo.model.Task;
import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.repository.TaskListRepository;
import cl.azurian.todo.repository.TaskRepository;
import cl.azurian.todo.util.FailHelper;
import cl.azurian.todo.vo.TaskListVO;
import cl.azurian.todo.vo.TaskVO;

@ExtendWith(MockitoExtension.class)
class DefaultTodoServiceTest {

	private static final Logger LOG = LoggerFactory.getLogger(DefaultTodoServiceTest.class);

	@InjectMocks
	private DefaultTodoService service;

	@Mock
	private TaskListRepository taskListRepository;
	@Mock
	private TaskRepository taskRepository;
	@Mock
	private ConversionService pulentodoConversionService;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testFindTaskListsForEmptyList() {
		when(taskListRepository.findAll()).thenReturn(Collections.emptyList());

		Collection<TaskListVO> actual = null;

		try {
			actual = service.findTaskLists();
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);
		assertTrue(actual.isEmpty());

		verify(taskListRepository).findAll();
		verifyNoInteractions(pulentodoConversionService);
	}

	@Test
	void testFindTaskListsForNonEmptyList() {
		when(taskListRepository.findAll()).thenReturn(Arrays.asList(new TaskList()));
		when(pulentodoConversionService.convert(any(TaskList.class), eq(TaskListVO.class)))
				.thenReturn(new TaskListVO(Integer.valueOf(0), "mock list", Collections.emptySet()));

		Collection<TaskListVO> actual = null;

		try {
			actual = service.findTaskLists();
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);
		assertFalse(actual.isEmpty());
		assertEquals(1, actual.size());

		verify(taskListRepository).findAll();
		verify(pulentodoConversionService).convert(any(TaskList.class), eq(TaskListVO.class));
	}

	@Test
	void testFindTaskListsForException() {
		when(taskListRepository.findAll()).thenThrow(new RuntimeException());

		assertThrows(RuntimeException.class, () -> service.findTaskLists());

		verify(taskListRepository).findAll();
		verifyNoInteractions(pulentodoConversionService);
	}

	@Test
	void testFindTaskListById() {
		when(taskListRepository.findById(any(Integer.class))).thenReturn(Optional.of(new TaskList()));
		when(pulentodoConversionService.convert(any(TaskList.class), eq(TaskListVO.class)))
				.thenReturn(new TaskListVO(Integer.valueOf(0), "mock list", Collections.emptySet()));

		TaskListVO actual = null;

		try {
			actual = service.findTaskListById(0);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);

		verify(taskListRepository).findById(any(Integer.class));
		verify(pulentodoConversionService).convert(any(TaskList.class), eq(TaskListVO.class));
	}

	@Test
	void testFindTaskListByIdForTaskListNotFound() {
		when(taskListRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

		assertThrows(TodoException.class, () -> service.findTaskListById(0));

		verify(taskListRepository).findById(any(Integer.class));
	}

	@Test
	void testFindTaskListByIdForException() {
		when(taskListRepository.findById(any(Integer.class))).thenThrow(new RuntimeException());

		assertThrows(RuntimeException.class, () -> service.findTaskListById(0));

		verify(taskListRepository).findById(any(Integer.class));
	}

	@Test
	void testSaveList() {
		TaskList mockEntity = new TaskList();
		mockEntity.setId(Integer.valueOf(0));
		mockEntity.setName("mock entity");
		mockEntity.setTasks(Collections.emptySet());

		when(taskListRepository.save(any(TaskList.class))).thenReturn(mockEntity);
		when(pulentodoConversionService.convert(any(TaskList.class), eq(TaskListVO.class)))
				.thenReturn(new TaskListVO(Integer.valueOf(0), "mock list", Collections.emptySet()));

		TaskListVO actual = null;

		try {
			actual = service.saveList(mockEntity);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);

		verify(taskListRepository).save(any(TaskList.class));
		verify(pulentodoConversionService).convert(any(TaskList.class), eq(TaskListVO.class));
	}

	@Test
	void testSaveListForException() {
		TaskList mockEntity = new TaskList();
		mockEntity.setId(Integer.valueOf(0));
		mockEntity.setName("mock entity");
		mockEntity.setTasks(Collections.emptySet());

		when(taskListRepository.save(any(TaskList.class))).thenThrow(new RuntimeException());

		assertThrows(RuntimeException.class, () -> service.saveList(mockEntity));

		verify(taskListRepository).save(any(TaskList.class));
		verifyNoInteractions(pulentodoConversionService);
	}

	@Test
	void testSaveTask() {
		TaskVO inputTask = new TaskVO(Integer.valueOf(0), "mock task", true, 0);

		when(taskListRepository.findById(anyInt())).thenReturn(Optional.of(new TaskList()));
		when(taskRepository.save(any(Task.class))).thenReturn(new Task());
		when(pulentodoConversionService.convert(any(Task.class), eq(TaskVO.class))).thenReturn(inputTask);

		TaskVO actual = null;

		try {
			actual = service.saveTask(inputTask);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);

		verify(taskListRepository).findById(anyInt());
		verify(taskRepository).save(any(Task.class));
		verify(pulentodoConversionService).convert(any(Task.class), eq(TaskVO.class));
	}

	@Test
	void testSaveTaskForException() {
		TaskVO inputTask = new TaskVO(Integer.valueOf(0), "mock task", true, 0);

		when(taskListRepository.findById(anyInt())).thenThrow(new RuntimeException());

		assertThrows(RuntimeException.class, () -> service.saveTask(inputTask));

		verify(taskListRepository).findById(anyInt());
		verifyNoInteractions(taskRepository);
		verifyNoInteractions(pulentodoConversionService);
	}

	@Test
	void testFindTask() {
		when(taskRepository.findById(Integer.valueOf(0))).thenReturn(Optional.of(new Task()));
		when(pulentodoConversionService.convert(any(Task.class), eq(TaskVO.class)))
				.thenReturn(new TaskVO(Integer.valueOf(0), "mock task", false, Integer.valueOf(0)));

		TaskVO actual = null;
		try {
			actual = service.findTask(0);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);

		verify(taskRepository).findById(anyInt());
		verify(pulentodoConversionService).convert(any(Task.class), eq(TaskVO.class));
	}

	@Test
	void testFindTaskForException() {
		when(taskRepository.findById(Integer.valueOf(0))).thenThrow(new RuntimeException());

		assertThrows(RuntimeException.class, () -> service.findTask(0));

		verify(taskRepository).findById(anyInt());
		verifyNoInteractions(pulentodoConversionService);
	}

}
