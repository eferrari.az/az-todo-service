package cl.azurian.todo.model;

import static org.junit.jupiter.api.Assertions.fail;

import java.util.Collections;
import java.util.Set;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.beanrunner.BeanRunner;

public class ModelGetterAndSetterTest {

	private static final String FAIL_MESSAGE = "Error running bean [%s]: %s";

	private static final Logger LOG = LoggerFactory.getLogger(ModelGetterAndSetterTest.class);

	private static BeanRunner beanRunner;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		beanRunner = new BeanRunner();
		enrichRunner(beanRunner);
	}

	private static void enrichRunner(BeanRunner runner) {
		runner.addTestValue(Set.class, Collections.emptySet());
	}

	private void run(Object bean) {
		LOG.info(String.format("Testing instance of %s", bean.getClass().getName()));
		try {
			beanRunner.testBean(bean);
			bean.toString();
		} catch (Exception e) {
			LOG.error("Error ejecutando testComuna()", e);
			fail(String.format(FAIL_MESSAGE, bean.getClass().getName(), e.getMessage()));
		}
	}

	@Test
	void testTask() {
		run(new Task());
	}

	@Test
	void testTaskList() {
		run(new TaskList());
	}

}
