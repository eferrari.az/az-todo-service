package cl.azurian.todo.util;

import static org.junit.jupiter.api.Assertions.fail;

import org.slf4j.Logger;

public class FailHelper {

	public static final String UNEXPECTED_ERROR_MSG = "Unexpected error";
	public static final String UNEXPECTED_ERROR_FAIL = UNEXPECTED_ERROR_MSG + " %s: %s";

	public static void handleUnexpectedFailure(Logger log, Exception e) {
		log.error(FailHelper.UNEXPECTED_ERROR_MSG, e);
		fail(String.format(FailHelper.UNEXPECTED_ERROR_FAIL, e.getClass().getName(), e.getMessage()));
	}
}
