package cl.azurian.todo.converters;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashSet;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

import cl.azurian.todo.model.Task;
import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.util.FailHelper;
import cl.azurian.todo.vo.TaskListVO;
import cl.azurian.todo.vo.TaskVO;

@ExtendWith(MockitoExtension.class)
class TaskListConverterTest {

	private static final Logger LOG = LoggerFactory.getLogger(TaskListConverterTest.class);

	@InjectMocks
	private TaskListConverter converter;

	@Mock
	private Converter<Task, TaskVO> taskConverter;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testConvertForEmptyTasks() {
		TaskList input = new TaskList();
		input.setId(Integer.valueOf(0));
		input.setName("mock list");
		input.setTasks(Collections.emptySet());

		TaskListVO actual = null;
		try {
			actual = converter.convert(input);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);
		assertNotNull(actual.getTasks());
		assertTrue(actual.getTasks().isEmpty());

		verifyNoInteractions(taskConverter);
	}

	@Test
	void testConvert() {
		when(taskConverter.convert(any(Task.class)))
				.thenReturn(new TaskVO(Integer.valueOf(0), "mock task", false, Integer.valueOf(0)));

		TaskList input = new TaskList();
		input.setId(Integer.valueOf(0));
		input.setName("mock list");
		input.setTasks(new HashSet<>());

		input.getTasks().add(new Task());

		TaskListVO actual = null;
		try {
			actual = converter.convert(input);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);
		assertNotNull(actual.getTasks());
		assertFalse(actual.getTasks().isEmpty());

		verify(taskConverter).convert(any(Task.class));
	}

}
