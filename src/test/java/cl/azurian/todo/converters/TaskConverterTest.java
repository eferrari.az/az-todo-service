package cl.azurian.todo.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cl.azurian.todo.model.Task;
import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.util.FailHelper;
import cl.azurian.todo.vo.TaskVO;

class TaskConverterTest {

	private static final Logger LOG = LoggerFactory.getLogger(TaskConverterTest.class);

	private static TaskConverter converter;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		converter = new TaskConverter();
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testConvert() {
		TaskVO actual = null;
		Task input = new Task();
		input.setId(0);
		input.setName("mock task");
		input.setDone(false);
		TaskList list = new TaskList();
		input.setTaskList(list);

		try {
			actual = converter.convert(input);
		} catch (Exception e) {
			FailHelper.handleUnexpectedFailure(LOG, e);
		}

		assertNotNull(actual);
		assertEquals(input.getId(), actual.getId());
		assertEquals(input.getName(), actual.getName());
		assertEquals(input.isDone(), actual.isDone());
		assertEquals(input.getTaskList().getId(), actual.getListId());
	}

}
