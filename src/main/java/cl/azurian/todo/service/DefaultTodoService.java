package cl.azurian.todo.service;

import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cl.azurian.todo.exceptions.TodoException;
import cl.azurian.todo.model.Task;
import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.repository.TaskListRepository;
import cl.azurian.todo.repository.TaskRepository;
import cl.azurian.todo.vo.TaskListVO;
import cl.azurian.todo.vo.TaskVO;

@Service
public class DefaultTodoService implements TodoService {

	private TaskListRepository taskListRepository;
	private TaskRepository taskRepository;

	private ConversionService pulentodoConversionService;

	public DefaultTodoService(TaskListRepository taskListRepository, TaskRepository taskRepository,
			ConversionService pulentodoConversionService) {
		this.taskListRepository = taskListRepository;
		this.taskRepository = taskRepository;

		this.pulentodoConversionService = pulentodoConversionService;
	}

	@Transactional(readOnly = true)
	@Override
	public Collection<TaskListVO> findTaskLists() {
		return taskListRepository.findAll().stream()
				.map(list -> pulentodoConversionService.convert(list, TaskListVO.class))
				.collect(Collectors.toList());
	}

	@Transactional(readOnly = true)
	@Override
	public TaskListVO findTaskListById(int id) {
		return pulentodoConversionService.convert(
				taskListRepository.findById(id).orElseThrow(() -> new TodoException("Task not found")),
				TaskListVO.class);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public TaskListVO saveList(TaskList taskList) {
		return pulentodoConversionService.convert(taskListRepository.save(taskList), TaskListVO.class);
	}

	@Transactional(propagation = Propagation.REQUIRED)
	@Override
	public TaskVO saveTask(TaskVO task) {
		TaskList taskListEntity = taskListRepository.findById(task.getListId()).orElseThrow(() -> new TodoException("list not found"));
		
		Task newTask = new Task();
		newTask.setName(task.getName());
		newTask.setDone(task.isDone());
		newTask.setTaskList(taskListEntity);
		return pulentodoConversionService.convert(taskRepository.save(newTask), TaskVO.class);
	}

	@Transactional(readOnly = true)
	@Override
	public TaskVO findTask(int id) {
		return pulentodoConversionService.convert(
				taskRepository.findById(id).orElseThrow(() -> new TodoException("Task not found")), TaskVO.class);
	}

}
