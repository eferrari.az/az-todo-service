package cl.azurian.todo.service;

import java.util.Collection;

import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.vo.TaskListVO;
import cl.azurian.todo.vo.TaskVO;

public interface TodoService {

	Collection<TaskListVO> findTaskLists();

	TaskListVO findTaskListById(int id);

	TaskListVO saveList(TaskList taskList);

	TaskVO saveTask(TaskVO task);

	TaskVO findTask(int id);

}
