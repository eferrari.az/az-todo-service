package cl.azurian.todo.vo;

import java.io.Serializable;
import java.util.Set;

public class TaskListVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private Set<TaskVO> tasks;

	public TaskListVO(Integer id, String name, Set<TaskVO> tasks) {
		this.id = id;
		this.name = name;
		this.tasks = tasks;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<TaskVO> getTasks() {
		return tasks;
	}

	public void setTasks(Set<TaskVO> tasks) {
		this.tasks = tasks;
	}

	@Override
	public String toString() {
		return "TaskListVO [id=" + id + ", name=" + name + ", tasks=" + tasks + "]";
	}
}
