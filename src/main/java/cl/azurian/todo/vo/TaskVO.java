package cl.azurian.todo.vo;

import java.io.Serializable;

public class TaskVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String name;
	private Boolean done;
	private Integer listId;

	public TaskVO(Integer id, String name, Boolean done, Integer listId) {
		super();
		this.id = id;
		this.name = name;
		this.done = done;
		this.listId = listId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean isDone() {
		return done;
	}

	public void setDone(Boolean done) {
		this.done = done;
	}

	public Integer getListId() {
		return listId;
	}

	public void setListId(Integer listId) {
		this.listId = listId;
	}

	@Override
	public String toString() {
		return "TaskVO [id=" + id + ", name=" + name + ", done=" + done + "]";
	}

}
