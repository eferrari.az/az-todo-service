package cl.azurian.todo.controller;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.service.TodoService;
import cl.azurian.todo.vo.TaskListVO;

@RestController
public class TaskListController {

	private static final Logger LOG = LoggerFactory.getLogger(TaskListController.class);

	private TodoService todoService;

	public TaskListController(TodoService todoService) {
		this.todoService = todoService;
	}

	@GetMapping("/lists")
	public ResponseEntity<Collection<TaskListVO>> showAll() {
		try {
			Collection<TaskListVO> lists = todoService.findTaskLists();
			return ResponseEntity.ok(lists);
		} catch (Exception e) {
			LOG.error("Error getting all tasks lists", e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@GetMapping("/list/{id}")
	public ResponseEntity<TaskListVO> showList(@PathVariable("id") int listId) {
		try {
			return ResponseEntity.ok(todoService.findTaskListById(listId));
		} catch (Exception e) {
			LOG.error("Error getting list {}", listId, e);
			return ResponseEntity.internalServerError().build();
		}
	}

	@PostMapping("/list/save")
	public ResponseEntity<TaskListVO> save(@RequestBody String listName) {
		try {
			TaskList newList = new TaskList();
			newList.setName(listName);

			return ResponseEntity.ok(todoService.saveList(newList));
		} catch (Exception e) {
			LOG.error("Error saving new list '{}'", listName, e);
			return ResponseEntity.internalServerError().build();
		}
	}

}
