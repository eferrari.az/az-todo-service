package cl.azurian.todo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cl.azurian.todo.service.TodoService;
import cl.azurian.todo.vo.TaskVO;

@RestController
public class TaskController {

	private static final Logger LOG = LoggerFactory.getLogger(TaskController.class);

	private TodoService todoService;

	public TaskController(TodoService todoService) {
		this.todoService = todoService;
	}
	
	@PostMapping("/task")
	public ResponseEntity<TaskVO> save(@RequestBody TaskVO task) {
		TaskVO updatedTask = null;
		try {
			updatedTask = todoService.saveTask(task);
		} catch (Exception e) {
			LOG.error("Error updating task {}", task.getName(), e);
			return ResponseEntity.internalServerError().build();
		}
		return ResponseEntity.ok(updatedTask);
	}

}
