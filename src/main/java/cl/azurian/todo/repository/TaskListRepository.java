package cl.azurian.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.azurian.todo.model.TaskList;

public interface TaskListRepository extends JpaRepository<TaskList, Integer> {

}
