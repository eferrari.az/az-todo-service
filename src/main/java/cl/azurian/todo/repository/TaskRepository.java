package cl.azurian.todo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cl.azurian.todo.model.Task;

public interface TaskRepository extends JpaRepository<Task, Integer> {

}
