package cl.azurian.todo.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.azurian.todo.model.Task;
import cl.azurian.todo.vo.TaskVO;

@Component
public class TaskConverter implements Converter<Task, TaskVO> {

	@Override
	public TaskVO convert(Task source) {
		return new TaskVO(source.getId(), source.getName(), source.isDone(), source.getTaskList().getId());
	}

}
