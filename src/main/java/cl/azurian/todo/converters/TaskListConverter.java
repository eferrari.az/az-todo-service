package cl.azurian.todo.converters;

import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import cl.azurian.todo.model.Task;
import cl.azurian.todo.model.TaskList;
import cl.azurian.todo.vo.TaskListVO;
import cl.azurian.todo.vo.TaskVO;

@Component
public class TaskListConverter implements Converter<TaskList, TaskListVO> {

	private Converter<Task, TaskVO> taskConverter;

	public TaskListConverter(Converter<Task, TaskVO> taskConverter) {
		this.taskConverter = taskConverter;
	}

	@Override
	public TaskListVO convert(TaskList source) {
		Set<TaskVO> convertedTasks;
		if (source.getTasks().isEmpty()) {
			convertedTasks = Collections.emptySet();
		} else {
			convertedTasks = source.getTasks()
					.stream()
					.map(task -> taskConverter.convert(task))
					.collect(Collectors.toSet());
		}

		return new TaskListVO(source.getId(), source.getName(), convertedTasks);
	}

}
