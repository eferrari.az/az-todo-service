package cl.azurian.todo.config;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;

@Configuration
@ComponentScan(basePackages = "cl.azurian.pulentodo")
public class PulentodoConfig {

	@Autowired
	private Set<Converter<?, ?>> converterList;

	@Bean
	public ConversionService pulentodoConversionService() {
		ConversionServiceFactoryBean bean = new ConversionServiceFactoryBean();
		bean.setConverters(getConverters());
		bean.afterPropertiesSet();

		return bean.getObject();
	}

	private Set<Converter<?, ?>> getConverters() {
		return new HashSet<>(converterList);
	}
}
