package cl.azurian.todo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PulentodoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PulentodoApplication.class, args);
	}

}
