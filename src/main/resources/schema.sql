DROP TABLE tasks IF EXISTS;
DROP TABLE task_lists IF EXISTS;

CREATE TABLE tasks (
  id INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(100),
  done BOOLEAN,
  task_list_id INTEGER
);

CREATE TABLE task_lists (
  id   INTEGER IDENTITY PRIMARY KEY,
  name VARCHAR(100)
);

ALTER TABLE tasks ADD CONSTRAINT fk_task_list FOREIGN KEY (task_list_id) REFERENCES task_lists (id);
