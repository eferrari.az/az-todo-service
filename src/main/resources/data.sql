INSERT INTO task_lists VALUES (1, 'Lista 1');
INSERT INTO task_lists VALUES (2, 'Lista 2');
INSERT INTO task_lists VALUES (3, 'Lista 3');
INSERT INTO task_lists VALUES (4, 'Lista 4');
INSERT INTO task_lists VALUES (5, 'Lista 5');
INSERT INTO task_lists VALUES (6, 'Lista 6');

INSERT INTO tasks VALUES (1, 'Tarea 1.1', TRUE, 1);
INSERT INTO tasks VALUES (2, 'Tarea 1.2', TRUE, 1);
INSERT INTO tasks VALUES (3, 'Tarea 1.3', TRUE, 1);
INSERT INTO tasks VALUES (4, 'Tarea 1.4', FALSE, 1);

INSERT INTO tasks VALUES (5, 'Tarea 2.1', TRUE, 2);
INSERT INTO tasks VALUES (6, 'Tarea 2.2', TRUE, 2);
INSERT INTO tasks VALUES (7, 'Tarea 2.3', FALSE, 2);
INSERT INTO tasks VALUES (8, 'Tarea 2.4', FALSE, 2);

INSERT INTO tasks VALUES (9, 'Tarea 3.1', FALSE, 3);
INSERT INTO tasks VALUES (10, 'Tarea 3.2', FALSE, 3);
INSERT INTO tasks VALUES (11, 'Tarea 3.3', FALSE, 3);
INSERT INTO tasks VALUES (12, 'Tarea 3.4', FALSE, 3);

INSERT INTO tasks VALUES (13, 'Tarea 4.1', FALSE, 4);
INSERT INTO tasks VALUES (14, 'Tarea 4.2', FALSE, 4);
INSERT INTO tasks VALUES (15, 'Tarea 4.3', FALSE, 4);
INSERT INTO tasks VALUES (16, 'Tarea 4.4', FALSE, 4);

INSERT INTO tasks VALUES (17, 'Tarea 5.1', FALSE, 5);
INSERT INTO tasks VALUES (18, 'Tarea 5.2', FALSE, 5);
INSERT INTO tasks VALUES (19, 'Tarea 5.3', FALSE, 5);
INSERT INTO tasks VALUES (20, 'Tarea 5.4', FALSE, 5);

INSERT INTO tasks VALUES (21, 'Tarea 6.1', FALSE, 6);
INSERT INTO tasks VALUES (22, 'Tarea 6.2', FALSE, 6);
INSERT INTO tasks VALUES (23, 'Tarea 6.3', FALSE, 6);
INSERT INTO tasks VALUES (24, 'Tarea 6.4', FALSE, 6);
