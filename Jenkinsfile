pipeline {
	agent any
	
	tools {
		maven "maven3"
	}
	
	environment {
		def pkgName = 'azurian-todo-service'
	}
	
	stages {
		stage('build & test') {
			steps {
				sh 'mvn clean test jacoco:prepare-agent jacoco:report'
			}
		}
		stage('SonarQube') {
			steps {
				withSonarQubeEnv('SonarQube lab') {
					script {
						def fixedBranchName = env.GIT_BRANCH.replace("origin/", "").replace("/", "_")
						sh 'mvn sonar:sonar -Dsonar.projectName=$pkgName:' + fixedBranchName + ' -Dsonar.projectKey=$pkgName:' + fixedBranchName
					}
				}
			}
		}
		stage('SonarQube Quality Gate') {
			steps {
				echo 'Esperando respuesta analisis SonarQube'
				timeout(5) {
					waitForQualityGate abortPipeline: false, credentialsId: 'sonarqube-token'
				}
			}
		}
		stage('package') {
			when {
				branch 'master'
			}
			steps {
				sh 'mvn package -DskipTests'
			}
		}
		stage('save artifacts') {
			when {
				branch 'master'
			}
			steps {
				archiveArtifacts artifacts: 'target/*.jar', followSymlinks: false
			}
		}
	}
	
	post {
		always {
			echo 'Build finalizado exitosamente'
		}
	}
}
